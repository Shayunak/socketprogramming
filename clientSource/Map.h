#ifndef SHOW_MAP
#define SHOW_MAP
#include "../client_config.h"

void showMap(int Cubes[MX_MIN_ONE][MX_MIN_ONE],int horizontalLines[MAX_SIZE][MX_MIN_ONE],int verticalLines[MX_MIN_ONE][MAX_SIZE], int mapSize);
void changeMap(move* userMove, int HorizontalLines[MAX_SIZE][MX_MIN_ONE], int VerticalLines[MX_MIN_ONE][MAX_SIZE]);
void showEvenRow(int horizontalLines[MAX_SIZE][MX_MIN_ONE] , int mapSize , int i);
void showOddRow(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int verticalLines[MX_MIN_ONE][MAX_SIZE] , int mapSize , int i);

#endif