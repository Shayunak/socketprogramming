#include "gameConditions.h"

int endOfGame(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int numberOfCubes) {
	int i, j;
	for (i = 0;i < numberOfCubes;i++){
		for (j = 0;j < numberOfCubes;j++){
			if (Cubes[i][j] == NOT_OCCUPIED)
				return 0;
		}
	}
	return 1;
}
int newSquare(int HorizontalLines[MAX_SIZE][MX_MIN_ONE], int VerticalLines[MX_MIN_ONE][MAX_SIZE],int turn,int row,int column) {
	if (HorizontalLines[row][column] && HorizontalLines[row+1][column] && VerticalLines[row][column] && VerticalLines[row][column+1])
		return turn + 1;
	else
		return NOT_OCCUPIED;
			
}

void calculateScores(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int numberOfCubes , int scores[]) {
	int i, j;
	
	for (i = 0;i < numberOfCubes;i++)
		for (j = 0;j < numberOfCubes;j++)
			scores[Cubes[i][j] - 1]++;
}

void showScores(int scores[] , int numberOfPlayers){
	int i;
	for(i = 0 ; i < numberOfPlayers ; i++)
		printf("Player%d score: %d\n" , i + 1 , scores[i]);
}

void showWinners(int winner , int winningState){
	if(winningState == ST_TIE)
		printf("No Winner!!Tie\n");
	else
		printf("The Winner is player%d\n" , winner);

}

int findMaximumScore(int scores[] , int numberOfPlayers){
	int i;
	int max = 0;
	for(i = 0 ; i < numberOfPlayers ; i++){
		if(max < scores[i])
			max = scores[i];
	}
	return max;
}

int findWinners(int scores[] , int numberOfPlayers , int* winner){
	int maximumScore = findMaximumScore(scores , numberOfPlayers);
	int i;
	*winner = NO_PLAYER;
	for(i = 0 ; i < numberOfPlayers; i++){
		if(scores[i] == maximumScore && (*winner == NO_PLAYER) )
			*winner = i + 1;
		else if(scores[i] == maximumScore && (*winner != NO_PLAYER) )
			return ST_TIE;
	}
	return ST_WIN;
}

int checkNewVerticalSquares(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int HorizontalLines[MAX_SIZE][MX_MIN_ONE] , int VerticalLines[MX_MIN_ONE][MAX_SIZE],int turn , int mapSize , move* userMove){
	int isNewSquare = NOT_OCCUPIED;
	if (userMove->row > 1) {
		Cubes[userMove->row - 2][userMove->column - 1] = newSquare(HorizontalLines, VerticalLines, turn, userMove->row - 2, userMove->column - 1);
		isNewSquare |= Cubes[userMove->row - 2][userMove->column - 1];
	}
	if (userMove->row < mapSize) {
		Cubes[userMove->row - 1][userMove->column - 1] = newSquare(HorizontalLines, VerticalLines, turn, userMove->row - 1, userMove->column - 1);
		isNewSquare |= Cubes[userMove->row - 1][userMove->column - 1];
	}
	return isNewSquare;
}

int checkNewHorizontalSquares(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int HorizontalLines[MAX_SIZE][MX_MIN_ONE] , int VerticalLines[MX_MIN_ONE][MAX_SIZE],int turn,int mapSize , move* userMove){
	int isNewSquare = NOT_OCCUPIED;
	if (userMove->column > 1) {
		Cubes[userMove->row - 1][userMove->column - 2] = newSquare(HorizontalLines, VerticalLines, turn, userMove->row - 1, userMove->column - 2);
		isNewSquare |= Cubes[userMove->row - 1][userMove->column - 2];
	}
	if (userMove->column < mapSize) {
		Cubes[userMove->row - 1][userMove->column - 1] = newSquare(HorizontalLines, VerticalLines, turn,userMove->row - 1, userMove->column - 1);
		isNewSquare |= Cubes[userMove->row - 1][userMove->column - 1];
	}
	return isNewSquare;
}

int checkNewSquares(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int HorizontalLines[MAX_SIZE][MX_MIN_ONE] , int VerticalLines[MX_MIN_ONE][MAX_SIZE],int turn,int mapSize , move* userMove){
	if(userMove == NULL)
		return NOT_OCCUPIED;
	if(userMove->axis == AXS_HORIZ)
		return checkNewVerticalSquares(Cubes , HorizontalLines , VerticalLines , turn , mapSize , userMove);
	else
		return checkNewHorizontalSquares(Cubes , HorizontalLines , VerticalLines , turn , mapSize , userMove);
}

