#include "Map.h"

void showEvenRow(int horizontalLines[MAX_SIZE][MX_MIN_ONE] , int mapSize , int i){
	int j;
	for (j = 0; j < (2*mapSize - 1); j++) {
		if (j%2==0) {
			printf("*");
		}
		else{
			if (horizontalLines[i/2][(j-1)/2])
				printf("---");
			else
				printf("   ");
		}		
	}
}

void showOddRow(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int verticalLines[MX_MIN_ONE][MAX_SIZE] , int mapSize , int i){
	int j;
	for (j = 0; j < (2*mapSize - 1); j++) {
		if (j % 2 == 0) {
			if (verticalLines[(i-1)/2][j/2])
				printf("|");
			else 
				printf(" ");
		}else{
			if(Cubes[(i-1)/2][(j-1)/2] == NOT_OCCUPIED )
				printf("   ");
			else
				printf(" %d " , Cubes[(i-1)/2][(j-1)/2]);
		}
	}
}

void showMap(int Cubes[MX_MIN_ONE][MX_MIN_ONE],int horizontalLines[MAX_SIZE][MX_MIN_ONE],int verticalLines[MX_MIN_ONE][MAX_SIZE] , int mapSize) {
	int i;
	for (i = 0; i < (2*mapSize - 1); i++) {
		if (i%2==0)
			showEvenRow(horizontalLines , mapSize , i);
		else	
			showOddRow(Cubes , verticalLines , mapSize , i);
		printf("\n");
	}
}
void changeMap(move* userMove, int HorizontalLines[MAX_SIZE][MX_MIN_ONE], int VerticalLines[MX_MIN_ONE][MAX_SIZE]) {
	if(userMove == NULL)
		return;
	if (userMove->axis == AXS_HORIZ)
		HorizontalLines[userMove->row - 1][userMove->column - 1] = 1;
	else
		VerticalLines[userMove->row - 1][userMove->column - 1] = 1;
}