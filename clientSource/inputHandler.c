#include "inputHandler.h"

int signalMove = 0;

void SignalHandler(){
	signalMove = 1;
	printf("Unfortunately you are out of time!\n");
}

void setupSignal(){
	struct sigaction act;

	memset (&act, NULL_CHAR, sizeof(act));
	act.sa_sigaction = &SignalHandler;
	act.sa_flags = 0;
	sigaction(SIGALRM, &act, NULL);
}

int checkInputStructure(int axis , int row,int column , int mapSize) {
	if (axis != AXS_VERT && axis != AXS_HORIZ)
		return 0;
	if ((row <= mapSize && row >= 1) && (column <= (mapSize-1) && column >= 1) && axis == AXS_HORIZ )
		return 1;
	if ((row <= (mapSize-1) && row >= 1) && (column <= mapSize && column >= 1) && axis == AXS_VERT )
		return 1;
	return 0;
}

move* allocateSpaceForMoveStruct(int axis ,int row , int column , move* UserMove){
	if(UserMove == NULL)
		UserMove = (move*) malloc(sizeof(move) );
	
	if(UserMove == NULL){
		printf("Error Allocating Memory!!!\n");
		exit(EXIT_FAIL);
	}
	{
		UserMove->axis = axis;
		UserMove->column = column;
		UserMove->row = row;
	}
	return UserMove;
}

int checkInputRepitition(int VerticalLines[MX_MIN_ONE][MAX_SIZE] , int HorizontalLines[MAX_SIZE][MX_MIN_ONE] , int axis , int row , int column){
	if(HorizontalLines[row - 1][column - 1] && axis == AXS_HORIZ)
		return 0;
	if(VerticalLines[row - 1][column-1] && axis == AXS_VERT)
		return 0;
	
	return 1;
}


void tryToGetInputProperly(int VerticalLines[MX_MIN_ONE][MAX_SIZE] , int HorizontalLines[MAX_SIZE][MX_MIN_ONE] ,int* axis , int* row , int* column , int mapSize){
	while(1){
		scanf("%d %d %d" , axis , row , column);
		if(signalMove)
			break;
		if(!checkInputStructure(*axis , *row , *column , mapSize) ){
			printf("one of the axis,row or column input is out of the defined range,Try again: ");
			continue;
		}
		if(checkInputRepitition(VerticalLines , HorizontalLines , *axis , *row , *column) )
			break;
		else
			printf("This Input has been already used,Try again: ");		
	}
}


move* getInput(move* UserMove , int gameTurn , int mapSize, int VerticalLines[MX_MIN_ONE][MAX_SIZE] ,int  HorizontalLines[MAX_SIZE][MX_MIN_ONE]){
	int row,column,axis;
	printf("It is your turn player%d:" , gameTurn + 1);
	
	setupSignal();
	alarm(SIGALRM_SEC);
	tryToGetInputProperly(VerticalLines , HorizontalLines ,&axis , &row , &column , mapSize);
	alarm(0);
	if(signalMove){
		free(UserMove);
		signalMove = 0;
		return NULL;
	}
	return allocateSpaceForMoveStruct(axis , row , column , UserMove);
}

int getGameOption(){
	int option;
	printf("Please enter on of three game options:\n2-two player mode\n3-three player mode\n4-four player mode\n");
	while(1){
		scanf("%d" , &option);
		if(option >= 2 && option <= 4)
			break;
		else
			printf("Not in the defined Range,Try Again: ");
	}
	return option;
}
