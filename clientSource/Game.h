#ifndef GAME_H
#define GAME_H

#include "../client_config.h"

void StartGame(int gameSocket ,int gamePort , int myTurn , int logFd , int mapSize , int numberOfPlayers);
void endGame(int gameSocket , int mapSize , int logFd , int numberOfPlayers , int Cubes[MX_MIN_ONE][MX_MIN_ONE] );
void newGame(int gamePort , int myTurn , int numberOfPlayers , int mapSize , int logFd);


#endif