#ifndef INPT_HANDLER
#define INPT_HANDLER

#include "../client_config.h"

void SignalHandler();
void setupSignal();
int checkInputStructure(int axis,int row,int column,int mapSize);
int checkInputRepitition(int VerticalLines[MX_MIN_ONE][MAX_SIZE] , int HorizontalLines[MAX_SIZE][MX_MIN_ONE] , int axis , int row , int column);
move* getInput(move* UserMove , int gameTurn , int mapSize, int VerticalLines[MX_MIN_ONE][MAX_SIZE] ,int  HorizontalLines[MAX_SIZE][MX_MIN_ONE]);
move* allocateSpaceForMoveStruct(int axis ,int row , int column , move* UserMove);
void tryToGetInputProperly(int VerticalLines[MX_MIN_ONE][MAX_SIZE] , int HorizontalLines[MAX_SIZE][MX_MIN_ONE] ,int* axis , int* row , int* column , int mapSize);
int getGameOption();

#endif