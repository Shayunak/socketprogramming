#ifndef CL_CON_MNG
#define CL_CON_MNG

#include "../client_config.h"

int createSocketUDP(int logFd);
int createSocketTCP(int logFd);
void makePacket(char* packet , int newTurn , move* Move);
move* exctractPacket(char* packet , int* newTurn , move* Move);

void tryToBindForBroadCast(struct sockaddr_in* myAddr , int logFd , int sockId , int port);
void tryToBindForListening(struct sockaddr_in* myAddr , int logFd , int sockId , int port);

void setSocketOptionsBroadCast(int logFd , int sockId , int* option);
void setSocketOptionsListen(int logFd , int sockId , int* option);

int establishBroadcastConnection(int gamePort , int logFd);
int establishListenConnection(int gamePort , int logFd);

void broadCastMove(move* Move , int newTurn , int gamePort , int logFd);
void broadCast(char* packet , int gameSocket , int broadcastPort , int logFd);
move* recieveMove(move* Move , int* newTurn , int gameSocket , int gamePort , int logFd);

int establishServerConnection(int serverPort , int logFd);
void sendToServer(int numberOfPlayers , int serverSocket , int logFd);
void recieveFromServer(int* gamePort , int* myTurn ,int serverSocket , int logFd);
void extractServerPacket(char* packet , int* gamePort , int* myTurn);

#endif