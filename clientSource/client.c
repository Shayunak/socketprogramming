#include "../client_config.h"
#include "clientConnectionManager.h"
#include "inputHandler.h"
#include "Game.h"

int main(int argc , char* argv[]){
    int serverPort = atoi(argv[1]);
    int gamePort,myTurn;
    int logFd = open( CL_LOG_FILE , O_RDWR | O_CREAT);
    printf("Hello!Welcome to Dots-and-lines Game.\n");
    int numberOfPlayers = getGameOption();
    int serverSocket = establishServerConnection(serverPort , logFd);
    sendToServer(numberOfPlayers , serverSocket , logFd);
    recieveFromServer(&gamePort , &myTurn , serverSocket , logFd);
    close(serverSocket);
    newGame(gamePort , myTurn , numberOfPlayers , numberOfPlayers + 1 , logFd); /*mapsize:according to game Policy*/
    close(logFd);
    return 0;
}