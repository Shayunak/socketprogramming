#include "inputHandler.h"
#include "gameConditions.h"
#include "Map.h"
#include "Game.h"
#include "clientConnectionManager.h"

void endGame(int gameSocket , int mapSize , int logFd , int numberOfPlayers , int Cubes[MX_MIN_ONE][MX_MIN_ONE] ){
	int scores[MAX_OF_PLAYERS] = {0};
	int winner,winningState;
	calculateScores(Cubes, mapSize, scores);
	showScores(scores, numberOfPlayers);
	winningState = findWinners(scores,numberOfPlayers,&winner);
	showWinners(winner, winningState);
	printf("Good Game!Thank you for playing.\n");
}

void StartGame(int gameSocket, int gamePort , int myTurn , int logFd , int mapSize , int numberOfPlayers) {
	/*Initialization*/
	int gameTurn = 0;
	move* userMove = NULL;
	int Cubes[MX_MIN_ONE][MX_MIN_ONE] = { 0 };
	int HorizontalLines[MAX_SIZE][MX_MIN_ONE] = { 0 };
	int VerticalLines[MX_MIN_ONE][MAX_SIZE] = { 0 };
	/*Game Procedure*/
	while (!endOfGame(Cubes , mapSize - 1)) {
		showMap(Cubes,HorizontalLines,VerticalLines,mapSize);
		if(myTurn == gameTurn){
			userMove = getInput(userMove , gameTurn , mapSize , VerticalLines , HorizontalLines);
			changeMap(userMove,HorizontalLines,VerticalLines);
			if(checkNewSquares(Cubes,HorizontalLines,VerticalLines,gameTurn,mapSize,userMove) == NOT_OCCUPIED)
				gameTurn = (gameTurn + 1) % numberOfPlayers;
			broadCastMove(userMove , gameTurn , gamePort , logFd);
			userMove = recieveMove(userMove , &gameTurn , gameSocket , gamePort , logFd);/*fake recieve*/
		}else{
			printf("It is not your turn.It is player%d's turn.Please wait....\n" , gameTurn + 1);
			userMove = recieveMove(userMove , &gameTurn, gameSocket , gamePort ,logFd);
			changeMap(userMove,HorizontalLines,VerticalLines);
			checkNewSquares(Cubes,HorizontalLines,VerticalLines,gameTurn,mapSize,userMove);
		}
	}
	/*Game Finished*/
	showMap(Cubes, HorizontalLines, VerticalLines , mapSize);
	endGame(gameSocket , mapSize , logFd , numberOfPlayers , Cubes);
}

void newGame(int gamePort , int myTurn , int numberOfPlayers , int mapSize , int logFd){
    int gameSocket = establishListenConnection(gamePort, logFd);
    StartGame(gameSocket , gamePort , myTurn , logFd , mapSize , numberOfPlayers);
    close(gameSocket);
	write(logFd , "The UDP game socket is closed!\n" , 32);
}