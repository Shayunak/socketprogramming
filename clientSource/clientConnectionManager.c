#include "clientConnectionManager.h"
#include "inputHandler.h"


void makePacket(char* packet , int newTurn , move* Move){
    packet[0] = ITOC(newTurn);
    if(Move != NULL){
        packet[1] = DEL_PCKT;
        packet[2] = ITOC(Move->axis);   packet[3] = DEL_PCKT;
        packet[4] = ITOC(Move->row);    packet[5] = DEL_PCKT;
        packet[6] = ITOC(Move->column); packet[7] = NULL_CHAR;
    }else{
        packet[1] = NULL_CHAR;
    }
}

move* exctractPacket(char* packet , int* newTurn , move* Move){
    *newTurn = CTOI(packet[0]);
    if(strlen(packet) == 1)
        free(Move);
    else
        return allocateSpaceForMoveStruct(CTOI(packet[2]) ,CTOI(packet[4]), CTOI(packet[6]) , Move);
    return NULL;
}

int createSocketTCP(int logFd){
    int sockId;
    if((sockId = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) != -1){
        write(logFd , "TCP socket has been created successfully!\n" , 43);
    }else{
        write(logFd , "TCP socket could not be created!\n" , 34);
        write(logFd , "Process Terminated!\n" , 21);
        close(logFd);
        exit(EXIT_FAIL);
    }
    return sockId;
}

int createSocketUDP(int logFd){
    int sockId;
    if((sockId = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) != -1){
        write(logFd , "UDP socket has been created successfully!\n" , 43);
    }else{
        write(logFd , "socket could not be created!\n" , 30);
        write(logFd , "Process Terminated!\n" , 21);
        close(logFd);
        exit(EXIT_FAIL);
    }
    return sockId;
}

void setSocketOptionsBroadCast(int logFd , int sockId , int* option){
    if(setsockopt(sockId, SOL_SOCKET, SO_BROADCAST , option, sizeof(int) ) < 0){
        write(logFd , "Setting Broadcast Socket Options failed!\n" , 42);
        write(logFd , "Process Terminated!\n" , 21);
        close(sockId);
        close(logFd);
        exit(EXIT_FAIL);
    }
    write(logFd , "Setting Broadcast Socket Options was Successful!\n" , 50);
}

void setSocketOptionsListen(int logFd , int sockId , int* option){
    if(setsockopt(sockId, SOL_SOCKET, SO_BROADCAST | SO_REUSEPORT , option, sizeof(int) ) < 0){
        write(logFd , "Setting Listener Socket Options failed!\n" , 41);
        write(logFd , "Process Terminated!\n" , 21);
        close(sockId);
        close(logFd);
        exit(EXIT_FAIL);
    }
    write(logFd , "Setting Listener Socket Options was Successful!\n" , 49);
}

void tryToBindForBroadCast(struct sockaddr_in* myAddr , int logFd , int sockId , int port){
    myAddr->sin_family = AF_INET;
    myAddr->sin_port = htons(port);
    myAddr->sin_addr.s_addr = inet_addr(DEFAULT_IP);

    if(bind(sockId, (struct sockaddr*) myAddr, sizeof(struct sockaddr_in))<0){
        write(logFd , "Broadcast Binding has failed\n" , 30);
        write(logFd , "Process Terminated!\n" , 21);
        close(sockId);
        close(logFd);
        exit(EXIT_FAIL);
    }
    write(logFd , "Broadcast Binding has been successful\n" , 39);
}

void tryToBindForListening(struct sockaddr_in* myAddr , int logFd , int sockId , int port){
    myAddr->sin_family = AF_INET;
    myAddr->sin_port = htons(port);
    myAddr->sin_addr.s_addr = htonl(INADDR_BROADCAST);

    if(bind(sockId, (struct sockaddr*) myAddr, sizeof(struct sockaddr_in))<0){
        write(logFd , "Listen Binding has failed\n" , 27);
        write(logFd , "Process Terminated!\n" , 21);
        close(sockId);
        close(logFd);
        exit(EXIT_FAIL);
    }
    write(logFd , "Listen Binding has been successful\n" , 36);
}

int establishListenConnection(int gamePort , int logFd){
    int option = 1;
    struct sockaddr_in myAddr;
    int sockId = createSocketUDP(logFd);
    setSocketOptionsListen(logFd , sockId , &option);
    tryToBindForListening(&myAddr , logFd , sockId , gamePort);
    write(logFd , "Listen Connection has been successfully established!\n" ,54);
    return sockId;
}

int establishBroadcastConnection(int gamePort , int logFd){
    int option = 1;
    struct sockaddr_in myAddr;
    int sockId = createSocketUDP(logFd);
    setSocketOptionsBroadCast(logFd , sockId , &option);
    tryToBindForBroadCast(&myAddr , logFd , sockId , gamePort);
    write(logFd , "Broadcast Connection has been successfully established!\n" , 57);
    return sockId;
}

void broadCastMove(move* Move , int newTurn , int gamePort , int logFd){
    int broadcastSocket = establishBroadcastConnection(gamePort , logFd);
    char packet[MAX_PCKT_SIZE];
    makePacket(packet , newTurn , Move);
    broadCast(packet , broadcastSocket , gamePort , logFd);
    close(broadcastSocket);
    write(logFd, "Broadcast connection is closed!\n" , 33);
}

void broadCast(char* packet , int broadcastSocket , int gamePort , int logFd){
    struct sockaddr_in myAddr;
    myAddr.sin_family = AF_INET;
    myAddr.sin_port = htons(gamePort);
    myAddr.sin_addr.s_addr = inet_addr(BROADCAST_IP);
    int addrLen = sizeof(myAddr);
    
    sendto(broadcastSocket, packet,MAX_PCKT_SIZE, 0, (struct sockaddr*) &myAddr, addrLen);
    write(logFd, "Packet is Broadcasted!\n" , 24);
}

move* recieveMove(move* Move , int* newTurn , int gameSocket , int gamePort , int logFd){
    char packet[MAX_PCKT_SIZE];
    struct sockaddr_in myAddr;
    myAddr.sin_family = AF_INET;
    myAddr.sin_port = htons(gamePort);
    myAddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    int addrLen = sizeof(myAddr);
    
    recvfrom(gameSocket, packet , MAX_PCKT_SIZE , 0 , (struct sockaddr*) &myAddr , (socklen_t*) &addrLen);
    write(logFd, "Packet recieved!\n" , 18);
    return exctractPacket(packet , newTurn ,Move);
}

void extractServerPacket(char* packet , int* gamePort , int* myTurn){
    *myTurn = CTOI(packet[0]);
    *gamePort = atoi(&(packet[2]) );
}

void tryToConnectToServer(struct sockaddr_in* servAddr , int logFd , int serverPort , int sockId){
    servAddr->sin_family = AF_INET;
    servAddr->sin_port = htons(serverPort);
    servAddr->sin_addr.s_addr = inet_addr(DEFAULT_IP);
    if(connect(sockId , (struct sockaddr*) servAddr , sizeof(struct sockaddr_in) ) < 0){
        write(logFd , "Connection Blocked By Server!\n" , 31);
        write(logFd , "Process Terminated!\n" , 21);
        close(sockId);
        close(logFd);
        exit(EXIT_FAIL);
    }
    write(logFd , "Successfully connected to the server!\n" , 39);
}

int establishServerConnection(int serverPort , int logFd){
    int sockId = createSocketTCP(logFd);
    struct sockaddr_in servAddr;

    printf("Wait for the server to respond....\n");
    tryToConnectToServer(&servAddr , logFd , serverPort , sockId);
    write(logFd , "Connection to server successfully established!\n" , 48);
    return sockId;
}

void sendToServer(int numberOfPlayers , int serverSocket , int logFd){
    char packet[MAX_PCKT_SIZE];
    packet[0] = ITOC(numberOfPlayers);  packet[1] = NULL_CHAR;

    send(serverSocket , packet , MAX_PCKT_SIZE , 0);
    write(logFd , "Sent packet to server!\n" , 24);
}

void recieveFromServer(int* gamePort , int* myTurn ,int serverSocket , int logFd){
    char packet[MAX_PCKT_SIZE];
    
    recv(serverSocket , packet , MAX_PCKT_SIZE , 0);
    write(logFd , "Recieved Packet from Server!\n" , 30);
    extractServerPacket(packet , gamePort , myTurn);
}