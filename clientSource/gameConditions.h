#ifndef G_CNDS
#define G_CNDS

#include "../client_config.h"

/*Is there new square?*/
int checkNewVerticalSquares(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int HorizontalLines[MAX_SIZE][MX_MIN_ONE] , int VerticalLines[MX_MIN_ONE][MAX_SIZE],int turn , int mapSize , move* userMove);
int checkNewHorizontalSquares(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int HorizontalLines[MAX_SIZE][MX_MIN_ONE] , int VerticalLines[MX_MIN_ONE][MAX_SIZE],int turn,int mapSize , move* userMove);
int checkNewSquares(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int HorizontalLines[MAX_SIZE][MX_MIN_ONE] , int VerticalLines[MX_MIN_ONE][MAX_SIZE],int turn, int mapSize , move* userMove);
int newSquare(int HorizontalLines[MAX_SIZE][MX_MIN_ONE], int VerticalLines[MX_MIN_ONE][MAX_SIZE],int turn,int row,int column);
/*During the game*/
int endOfGame(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int mapSize);
/*Who is the Winner?*/
void calculateScores(int Cubes[MX_MIN_ONE][MX_MIN_ONE] , int mapSize , int scores[]);
void showScores(int scores[] , int numberOfPlayers);
void showWinners(int winner , int winningState);
int findWinners(int scores[] , int numberOfPlayers , int* winner);
int findMaximumScore(int scores[] , int numberOfPlayers);

#endif