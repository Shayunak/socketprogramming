CC = gcc -Wall -pedantic -Werror
BUILD_DIR = builds

all: $(BUILD_DIR) client server

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

server:$(BUILD_DIR)/server.o $(BUILD_DIR)/serverGroupHandler.o $(BUILD_DIR)/serverConnectionManager.o server_config.h
	${CC} $(BUILD_DIR)/serverConnectionManager.o $(BUILD_DIR)/server.o $(BUILD_DIR)/serverGroupHandler.o -o server

$(BUILD_DIR)/server.o: serverSource/server.c serverSource/serverConnectionManager.h serverSource/serverGroupHandler.h
	${CC} -c serverSource/server.c -o $(BUILD_DIR)/server.o

$(BUILD_DIR)/serverConnectionManager.o: serverSource/serverConnectionManager.c serverSource/serverConnectionManager.h
	${CC} -c serverSource/serverConnectionManager.c -o $(BUILD_DIR)/serverConnectionManager.o

$(BUILD_DIR)/serverGroupHandler.o: serverSource/serverGroupHandler.c serverSource/serverGroupHandler.h serverSource/serverConnectionManager.h
	${CC} -c serverSource/serverGroupHandler.c -o $(BUILD_DIR)/serverGroupHandler.o

client: $(BUILD_DIR)/client.o $(BUILD_DIR)/Game.o $(BUILD_DIR)/Map.o $(BUILD_DIR)/inputHandler.o $(BUILD_DIR)/gameConditions.o $(BUILD_DIR)/clientConnectionManager.o client_config.h
	${CC} $(BUILD_DIR)/Game.o $(BUILD_DIR)/clientConnectionManager.o $(BUILD_DIR)/Map.o $(BUILD_DIR)/gameConditions.o $(BUILD_DIR)/inputHandler.o $(BUILD_DIR)/client.o -o client

$(BUILD_DIR)/client.o: clientSource/client.c clientSource/Game.h clientSource/clientConnectionManager.h
	${CC} -c clientSource/client.c -o $(BUILD_DIR)/client.o

$(BUILD_DIR)/Game.o: clientSource/Game.c clientSource/Game.h clientSource/Map.h clientSource/inputHandler.h clientSource/gameConditions.h clientSource/clientConnectionManager.h
	${CC} -c clientSource/Game.c -o $(BUILD_DIR)/Game.o

$(BUILD_DIR)/inputHandler.o: clientSource/inputHandler.c clientSource/inputHandler.h
	${CC} -c clientSource/inputHandler.c -o $(BUILD_DIR)/inputHandler.o

$(BUILD_DIR)/Map.o: clientSource/Map.c clientSource/Map.h
	${CC} -c clientSource/Map.c -o $(BUILD_DIR)/Map.o

$(BUILD_DIR)/gameConditions.o: clientSource/gameConditions.c clientSource/gameConditions.h
	${CC} -c clientSource/gameConditions.c -o $(BUILD_DIR)/gameConditions.o

$(BUILD_DIR)/clientConnectionManager.o: clientSource/clientConnectionManager.c clientSource/clientConnectionManager.h clientSource/inputHandler.h
	${CC} -c clientSource/clientConnectionManager.c -o $(BUILD_DIR)/clientConnectionManager.o

.PHONY:clean
clean:
	rm -rf $(BUILD_DIR) client server