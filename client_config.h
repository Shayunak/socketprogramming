#ifndef CL_CNFG
#define CL_CNFG

#include <sys/types.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>  

#define DEL_PCKT '/'
#define NULL_CHAR '\0'
#define MAX_SIZE 5
#define MAX_PCKT_SIZE 10
#define MX_MIN_ONE 4
#define AXS_HORIZ 0
#define AXS_VERT 1
#define EXIT_FAIL 1
#define NO_NEW_SQUARE 0
#define NOT_OCCUPIED 0
#define MAX_OF_PLAYERS 4
#define ST_WIN 1
#define ST_TIE 0
#define NO_PLAYER 0

#define STDIN 0
#define STDOUT 1


#define DEFAULT_IP "127.0.0.1"
#define BROADCAST_IP "255.255.255.255"
#define SIGALRM_SEC 20
#define CL_LOG_FILE "clientLog.txt"

struct Move{
    int axis;
    int row;
    int column;
};

typedef struct  Move move;

#define ITOC(i) (i+'0')
#define CTOI(c) (c - '0')

#endif