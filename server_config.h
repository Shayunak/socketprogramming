#ifndef CL_CNFG
#define CL_CNFG

#include <sys/types.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>  

#define ITOC(i) (i+'0')
#define CTOI(c) (c - '0')

#define DEL_PCKT '/'
#define NULL_CHAR '\0'
#define EXIT_FAIL 1

#define START_PORT_GP2  10000 
#define START_PORT_GP3  20000
#define START_PORT_GP4  30000
#define DEFAULT_IP "127.0.0.1"
#define SV_LOG_FILE "serverLog.txt"
#define MAX_LISTEN_Q 20
#define MAX_PCKT_SIZE 10
#define PORT_NUM_DIGITS 5
#define MAX_FD_SELECT 23
#define MAX_SOCK_CON 1000
#define NO_SOCK -1
#define NUM_OPT 3
#define MAX_PLY 4
#define EXIT_FAIL 1

#define STDIN 0

#define ITOC(i) (i+'0')
#define CTOI(c) (c - '0')

typedef struct Player player;
typedef struct Group group;

struct Player {
    int socket;
    int socketsIndex;
};

struct Group {
    player* Players[MAX_PLY];
    int capacity;
    int matchedPlayers;
    int assignedPort;
};

#endif