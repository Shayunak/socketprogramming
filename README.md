# README #

### What is this repository for? ###

* Simple console lines&dots Game for OS course computer assignment based on sever-client IPC
* Server: Matches clients requested for 2,3,4 players modes via TCP socket connection
* Clients: After beign matched and port allocated by the server, they broadcast their actions on a UDP socket connection
* version 1.1

### How do I get set up? ###

* Just run "make all" command in terminal
* two executable files are produced : "client" and "server" 
* run "./client portX" to start your client
* run "./server portX" to start your server
* "portX" is the port you want the client and the server to communicate
* Warning: Run multiple clients in different directories, because logfile names will intervene eachother

### Contribution guidelines ###

* This project is developed in C language
* It has used multiple system calls(POSIX API based)
* it is multifile and each file is devoted to a functionality
* one folder for server source
* one folder for client source 
* You can alter sever definitions in file "server_config.h"
* You can alter client definitions in file "client_config.h"

### Who do I talk to? ###

* The OS course TA or anybody interested