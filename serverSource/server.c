#include "../server_config.h"
#include "serverConnectionManager.h"
#include "serverGroupHandler.h"

int main(int argc , char* argv[]){
    int servingPort = atoi(argv[1]);
    group Groups[NUM_OPT];
    /*initialization*/
    int logFd = open( SV_LOG_FILE , O_RDWR | O_CREAT);
    int servingSocket = initializeServer(logFd , servingPort);
    initializeGroups(Groups);
    int Sockets[MAX_SOCK_CON] = { NO_SOCK };
    fd_set socketSet;
    int numSockets = 0;
    /*service*/
    while(1){
        resetSocketSet(&socketSet , servingSocket , Sockets , numSockets);
        if(select(MAX_FD_SELECT , &socketSet , NULL , NULL , NULL) != 0){
            if (FD_ISSET(STDIN, &socketSet) ){ /* Check keyboard for shutting down server */
                getchar();
                break;
            }else{
                if(FD_ISSET(servingSocket , &socketSet) )
                    acceptClient(logFd , servingSocket , &numSockets , Sockets , &socketSet);
                for(int i = 0 ; i < numSockets ; i++){
                    if(Sockets[i] != NO_SOCK){
                        if(FD_ISSET(Sockets[i] , &socketSet) )
                            manageThisClient(Groups, &socketSet , logFd , Sockets , i , Sockets[i]);
                    }
                }
            }
        }
    }
    /*end*/
    closeAllRemainingSockets(Sockets);
    close(servingSocket);
    close(logFd);
    return 0;
}