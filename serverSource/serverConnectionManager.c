#include "serverConnectionManager.h"

void itos(char* output , int input){
    int i,d;
    for(i = 1 ; i <= PORT_NUM_DIGITS ; i++){
        d = input % 10;
        input = (input - d) / 10;
        output[PORT_NUM_DIGITS-i] = ITOC(d);
    }
    output[PORT_NUM_DIGITS] = NULL_CHAR;
}

int createSocketTCP(int logFd){
    int sockId;
    if((sockId = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) != -1){
        write(logFd , "TCP socket has been created successfully!\n" , 43);
    }else{
        write(logFd , "TCP socket could not be created!\n" , 34);
        write(logFd , "Process Terminated!\n" , 21);
        close(logFd);
        exit(EXIT_FAIL);
    }
    return sockId;
}

void bindServerToPort(struct sockaddr_in* myAddr , int logFd , int servingSocket , int servingPort){
    myAddr->sin_family = AF_INET;
    myAddr->sin_port = htons(servingPort);
    myAddr->sin_addr.s_addr = htonl(INADDR_ANY);

    if(bind(servingSocket , (struct sockaddr*) myAddr , sizeof(struct sockaddr_in) ) < 0 ){
        write(logFd , "Binding to Port Failed!\n" , 25);
        write(logFd , "Process Terminated!\n" , 21);
        close(logFd);
        close(servingSocket);
        exit(EXIT_FAIL);
    }
    write(logFd , "Server is successfully bound to port!\n" , 39);
}

void tryToListen(int servingSocket , int logFd){
    if(listen(servingSocket , MAX_LISTEN_Q) < 0){
        write(logFd , "Listen Failed!\n" , 16);
        write(logFd , "Process Terminated!\n" , 21);
        close(logFd);
        close(servingSocket);
        exit(EXIT_FAIL);
    }
    write(logFd , "Server is Listening...\n" , 24);
}

int initializeServer(int logFd , int servingPort){
    int servingSocket = createSocketTCP(logFd);
    struct sockaddr_in myAddr;

    bindServerToPort(&myAddr , logFd , servingSocket , servingPort);
    tryToListen(servingSocket , logFd);
    write(logFd , "Server is successfuly initialized!\n" , 36);
    return servingSocket;
}

void sendToClient(int playerTurn , int gamePort , int clientSocket , int logFd){
    char packet[MAX_PCKT_SIZE];
    char portStr[MAX_PCKT_SIZE];
    packet[0] = ITOC(playerTurn); packet[1] = DEL_PCKT;  packet[2] = NULL_CHAR;
    itos(portStr , gamePort);
    strcat(packet , portStr);

    send(clientSocket , packet , MAX_PCKT_SIZE , 0);
    write(logFd , "Sent packet to Client!\n" , 24);
}

int recieveFromClient(int clientSocket , int logFd){
    int option;
    char packet[MAX_PCKT_SIZE];
    
    recv(clientSocket , packet , MAX_PCKT_SIZE , 0);
    write(logFd , "Recieved Packet from Client!\n" , 30);
    option = CTOI(packet[0]);
    return option;
}

void acceptClient(int logFd , int servingSocket , int* numSockets , int Sockets[MAX_SOCK_CON] , fd_set* socketSet){
    struct sockaddr clientAddr;
    int lenAddr = sizeof(clientAddr);
    int newClientSocket = accept(servingSocket , &clientAddr , (socklen_t*) &lenAddr);
    write(logFd , "New Client has been accepted!\n" , 31);
    Sockets[*numSockets] = newClientSocket;
    *numSockets = *numSockets + 1;
    FD_SET(newClientSocket, socketSet);
}

void closeAllRemainingSockets(int Sockets[MAX_SOCK_CON]){
    int i;
    for(i = 0 ; i < MAX_SOCK_CON ; i++)
        if(Sockets[i] != NO_SOCK)
            close(Sockets[i]);
}

void resetSocketSet(fd_set* socketSet , int servingSocket , int Sockets[MAX_SOCK_CON] , int numSockets){
    FD_ZERO(socketSet);
    FD_SET(STDIN , socketSet);
    FD_SET(servingSocket , socketSet);
    for(int i = 0 ; i < numSockets ; i++) 
        if(Sockets[i] != NO_SOCK)
            FD_SET(Sockets[i] , socketSet);
}