#ifndef SV_CON_MNG
#define SV_CON_MNG

#include "../server_config.h"

void itos(char* output , int input);

int createSocketTCP(int logFd);
void bindServerToPort(struct sockaddr_in* myAddr , int logFd , int servingSocket , int servingPort);
int initializeServer(int logFd , int servingPort);
void tryToListen(int servingSocket , int logFd);
void sendToClient(int playerTurn , int gamePort , int clientSocket , int logFd);
int recieveFromClient(int clientSocket , int logFd);
void acceptClient(int logFd , int servingSocket , int* numSockets , int Sockets[MAX_SOCK_CON] , fd_set* socketSet);
void closeAllRemainingSockets(int Sockets[MAX_SOCK_CON]);
void resetSocketSet(fd_set* socketSet , int servingSocket , int Sockets[MAX_SOCK_CON] , int numSockets);

#endif