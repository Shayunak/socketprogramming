#ifndef SV_GP_HANDLER
#define SV_GP_HANDLER

#include "../server_config.h"

void initializeGroups(group Groups[NUM_OPT]);
void manageThisClient(group Groups[NUM_OPT] , fd_set* socketSet , int logFd , int Sockets[MAX_SOCK_CON] , int socketsIndex , int clientSocket);
void addPlayerToGroup(group* addedGroup , int socketsIndex , int clientSocket);
player* newPlayer(int socketsIndex , int clientSocket);
void checkGroup(group* checkingGroup , int logFd , fd_set* socketSet , int Sockets[MAX_SOCK_CON]);
void sendMessagesToClients(group* completeGroup , int logFd);
void renewGroup(group* newGroup , fd_set* socketSet , int Sockets[MAX_SOCK_CON] , int logFd);
player* deletePlayer(player* deletedPlayer , fd_set* socketSet , int Sockets[MAX_SOCK_CON] , int logFd);

#endif