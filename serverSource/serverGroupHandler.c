#include "serverGroupHandler.h"
#include "serverConnectionManager.h"

void initializeGroups(group Groups[NUM_OPT]){
    Groups[0].capacity = 2;  Groups[0].assignedPort = START_PORT_GP2; /*2 Players*/
    Groups[1].capacity = 3;  Groups[1].assignedPort = START_PORT_GP3; /*3 Players*/
    Groups[2].capacity = 4;  Groups[2].assignedPort = START_PORT_GP4; /*4 Players*/
    for(int i = 0 ; i < NUM_OPT ; i++){
        Groups[i].matchedPlayers = 0;
        for(int j = 0 ; j < Groups[i].capacity ; j++)
            Groups[i].Players[j] = NULL;
    }
}

player* newPlayer(int socketsIndex , int clientSocket){
    player* newPlayer = (player*) malloc(sizeof(player) );
    if(newPlayer == NULL){
        printf("Error Allocating Memory!!!\n");
        exit(EXIT_FAIL);
    }
    newPlayer->socketsIndex = socketsIndex;
    newPlayer->socket = clientSocket;
    return newPlayer;
}

void addPlayerToGroup(group* addedGroup , int socketsIndex , int clientSocket){
    player* newPlayerAllocated = newPlayer(socketsIndex , clientSocket);
    addedGroup->Players[addedGroup->matchedPlayers] = newPlayerAllocated;
    addedGroup->matchedPlayers++;
}

void sendMessagesToClients(group* completeGroup , int logFd){
    for(int i = 0 ; i < completeGroup->capacity ; i++)
        sendToClient(i , completeGroup->assignedPort , completeGroup->Players[i]->socket , logFd);
}

player* deletePlayer(player* deletedPlayer , fd_set* socketSet , int Sockets[MAX_SOCK_CON] , int logFd){
    Sockets[deletedPlayer->socketsIndex] = NO_SOCK;
    FD_CLR(deletedPlayer->socket , socketSet);
    close(deletedPlayer->socket);
    write(logFd , "Client Connection Has been successfully closed!\n" , 49);
    free(deletedPlayer);
    return NULL;
}

void renewGroup(group* newGroup , fd_set* socketSet , int Sockets[MAX_SOCK_CON] , int logFd){
    newGroup->assignedPort++;
    newGroup->matchedPlayers = 0;
    for(int i = 0 ; i < newGroup->capacity ; i++)
        newGroup->Players[i] = deletePlayer(newGroup->Players[i] , socketSet , Sockets , logFd);
}

void checkGroup(group* checkingGroup , int logFd , fd_set* socketSet , int Sockets[MAX_SOCK_CON]){
    if(checkingGroup->capacity == checkingGroup->matchedPlayers){
        sendMessagesToClients(checkingGroup , logFd);
        renewGroup(checkingGroup , socketSet , Sockets , logFd);
    }
}

void manageThisClient(group Groups[NUM_OPT] , fd_set* socketSet , int logFd , int Sockets[MAX_SOCK_CON] , int socketsIndex , int clientSocket){
    int option = recieveFromClient(clientSocket , logFd);
    group* addedGroup = &Groups[option - 2];
    addPlayerToGroup(addedGroup , socketsIndex , clientSocket);
    checkGroup(addedGroup , logFd , socketSet , Sockets);
}